var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/generate', function(req, res, next) {
  var createGraph = require('ngraph.graph');
  var graph = createGraph();
  //fs.readFile( __dirname + '/resources/pubkey/blk00052.txt', function (err, data) {
  fs.readFile( __dirname + '/resources/tx/blk01800.txt', function (err, data) {
    let lines = data.toString().split('\n');
    let simpleRenderer = 0;
    lines.forEach(line => { //10000
      if(simpleRenderer === 30000){ return; }
        simpleRenderer++;
        let token = line.split('|-|');
        let toNode = token[0];
        //console.debug('To node with id: ', toNode);
        let fromNode = token[token.length - 1];
        //console.debug('From node with id: ', fromNode);
        
        graph.beginUpdate();
        graph.addNode(toNode);
        graph.addNode(fromNode);
        //console.debug('Information node is: ', token.slice(1, token.length - 1));
        //self.graph.addLink(toNode, fromNode, token.slice(1, token.length - 1));
        graph.addLink(toNode, fromNode, token.slice(1));
        graph.endUpdate();
    });
    //****MOCK example graph
   // let generatGraph = require('ngraph.generators').balancedBinTree(10);
    //let generatGraph = require('ngraph.generators').grid(100, 100);
    var save = require('ngraph.tobinary');
    save(graph);
    //save(generatGraph);
  });
  
});

module.exports = router;
