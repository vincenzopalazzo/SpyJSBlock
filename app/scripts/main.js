'use strict';

import 'regenerator-runtime/runtime';
import BuildGraph from './BuildGraph';
import PainterGraph from './RendererGraph';
import GraphLayoutUtil from './GraphLayoutUtil'
import RenderGraph from 'ngraph.pixel'

window.main = async function(args){
    
    if(args === 'build-graph'){
        let buildGraph = new BuildGraph();
        //buildGraph.builderData();
        //let graph = buildGraph.getGraphData();
        //let layout = buildGraph.getLayoutData();

        let graph = buildGraph.getGraph(); //for PIXI -> 2d
        //graph = await buildGraph.buildGraph();
        
        //Store graph, this have need to nodejs
        //let layoutUtil = new GraphLayoutUtil(graph);
        //layout = layoutUtil.buildLayout();
        
        //FOR PIXI -> 2d
        let paintergraph = new PainterGraph(graph);
        paintergraph.painGraph();
        graph = await buildGraph.buildGraph();

        //PIXEL 2D
       /* let renderer = RenderGraph(graph, {
            is3d: false,
            link: function createLinkUI(link) {
                if (link.data === 'hidden'){ return;} 
                return {
                  fromColor: 0xBDBDBD,
                  toColor: 0x757575
                };
            },
            node: function createNodeUI(node) {
                if (node.data === 'hidden'){ return;}
                return {
                  color: 0x1967be,
                  size: 100
                };
            },
        });*/
    }

}
