'use strict';

import createGraph from 'ngraph.graph'
import DAOLocalFile from './DAOLoadFile';
import ajax from './lib/layout/ajax'
import BuildPixiGraphics from 'ngraph.pixi';
import renderGraph from 'ngraph.pixel'

const PATH_RES = 'resources/';

class BuildGraph{

    constructor(){
        self = this;
        this.graph = createGraph();
    }

    getGraph(){
      return this.graph;
    }

    async buildGraph(){
        self = this;
        this.dao = new DAOLocalFile();
        
        console.debug('************* GRAPH of Address *************');
        //let actualIndex = self.indexBlk;
        let actualIndex = 0;
        let pathInput = this.getNameFile(PATH_RES + 'pubkey/', '_idw.txt', actualIndex);
       // let pathInput = this.getNameFile(PATH_RES + 'tx/', '_tx.txt', actualIndex);
        console.debug('Name file: ', pathInput);
        while(pathInput != null){
            await this.parseringTheFile(pathInput, '|-|')
            actualIndex++;
            //pathInput = self.getNameFile(PATH_RES + 'tx/', '_tx.txt', actualIndex);
            pathInput = this.getNameFile(PATH_RES + 'pubkey/', '_idw.txt', actualIndex);
            if(actualIndex == 1){break;} //TODO TMP
            console.log('Name file: ', pathInput);
        }
        
        return self.graph;
    }

    async parseringTheFile(pathInput, stringToken){
        let fileBlkPromisse = this.dao.loadFileText(pathInput);
        await fileBlkPromisse.then(function(fileBlkData) {
                   // console.debug('File content: ', fileBlkData);
                    let lines = fileBlkData.split('\n');
                    let simpleRenderer = 0;
                    lines.forEach(line => { //10000
                       if(simpleRenderer === 8000000){ return; }
                        simpleRenderer++;
                        let token = line.split(stringToken);
                        let toNode = token[0];
                        //console.debug('To node with id: ', toNode);
                        let fromNode = token[token.length - 1];
                        //console.debug('From node with id: ', fromNode);
                        
                        self.graph.beginUpdate();

                        self.graph.addNode(toNode);
                        self.graph.addNode(fromNode);
                        //console.debug('Information node is: ', token.slice(1, token.length - 1));
                        self.graph.addLink(toNode, fromNode);
                        
                        self.graph.endUpdate();
                    });
                   // actualIndex++;
                    //pathInput = self.getNameFile(PATH_RES + 'tx/', '_tx.txt', actualIndex);
                }).catch(ex => console.error(ex));
    }

    getGraphData(){
      return this.graph;
    }

    getLayoutData(){
      return this.layout;
    }

    async builderData(){

      await Promise.all([
        ajax('data/positions.bin', { responseType: 'arraybuffer' }).then(this.toInt32Array),
        ajax('data/links.bin', { responseType: 'arraybuffer' }).then(this.toInt32Array),
        ajax('data/labels.json').then(this.toJson)
      ]).then(this.render);
    }

    toInt32Array(oReq) {
      return new Int32Array(oReq.response);
    }
    
    toJson(oReq) {
      return JSON.parse(oReq.responseText);
    }

    render(data) {
      var positions = data[0];
      var links = data[1];
      var labels = data[2];

      var graph = initGraphFromLinksAndLabels(links, labels);
      
      //---- PIXI 2d start
     /* let pixiGraphics = new BuildPixiGraphics(graph, {
        //Setting ngraph.pixi
        rendererOptions: {
          backgroundColor: 0xFFFFFF,
          antialias: true,
        },
        labelConf: {
          enable: false,
          text: 'Label to personal code'
        },
      });
      pixiGraphics.createNodeUI(require('./lib/createNodeUI'));
      pixiGraphics.renderNode(require('./lib/renderNode'));
      pixiGraphics.createLinkUI(require('./lib/createLinkUI'));
      pixiGraphics.renderLink(require('./lib/renderLink'));

      var layout = pixiGraphics.layout;
      // Set node positions.
      pixiGraphics.run();*/
      // PIXI 2d end
      //3d pixel
      var renderer = renderGraph(graph);
      var layout = renderer.layout;

      labels.forEach(function (label, index) {
        var nodeCount = index * 3;
        var x = positions[nodeCount + 0];
        var y = positions[nodeCount + 1];
        var z = positions[nodeCount + 2];
    
        layout.setNodePosition(label, x, y, z);
      });

      //pixiGraphics.stop();
      renderer.redraw();
      
      function initGraphFromLinksAndLabels(links, labels) {
        var srcIndex;
      
        var graph =  createGraph({ uniqueLinkId: false });
        labels.forEach(label => graph.addNode(label));
        links.forEach(processLink);
      
        return graph;
      
        function processLink(link) {
          if (link < 0) {
            srcIndex = -link - 1;
          } else {
            var toNode = link - 1;
            var fromId = labels[srcIndex];
            var toId = labels[toNode];
            graph.addLink(fromId, toId);
          }
        }
      }
    
    }

    getNameFile(pathInput, exstension, numberBlock) {
        if(numberBlock < 10){
          return pathInput + 'blk0000' + String(numberBlock) + exstension;
        }else if(numberBlock < 100){
          return pathInput + 'blk000' + String(numberBlock) + exstension;
        }else if (numberBlock < 1000){
          return pathInput + 'blk00' + String(numberBlock) + exstension;
        }else if (numberBlock < 10000){
          return pathInput + 'blk0' + String(numberBlock) + exstension;
        }else if (numberBlock < 100000) {
          return pathInput + 'blk0' + String(numberBlock) + exstension;
        }
        return null;
      }
}

export default BuildGraph;