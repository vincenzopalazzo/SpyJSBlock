// Copyright (c) 2018-2019 Vincenzo Palazzo vicenzopalazzodev@gmail.com
// Distributed under the Apache License Version 2.0 software license,
// see https://www.apache.org/licenses/LICENSE-2.0.txt

'use strict';

class DAOLocalFile{

    constructor(path){
        this.path = path;
    }

    async loadFile(path){
        this.configurationData;
        try {
            const response = await fetch(path);
            const data = await response.json();
            // console.debug('Response to path: ' + path);
            // console.debug(data);
            return data;
        }
        catch (ex) {
            return console.error(ex);
        }
    }

    async loadFileText(path){
        this.configurationData;
        try {
            const response = await fetch(path);
            const data = await response.text();
            // console.debug('Response to path: ' + path);
            //console.debug(data);
            return data;
        }
        catch (ex) {
            return console.error(ex);
        }
    }
}

export default DAOLocalFile;