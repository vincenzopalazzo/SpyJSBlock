'use strict';

import BuildPixiGraphics from 'ngraph.pixi';
import ManagerGraphEvent from './ManagerGraphEvent'

const niceColors = [
  '#1f77b4', '#aec7e8',
  '#ff7f0e', '#ffbb78',
  '#2ca02c', '#98df8a',
  '#d62728', '#ff9896',
  '#9467bd', '#c5b0d5',
  '#8c564b', '#c49c94',
  '#e377c2', '#f7b6d2',
  '#7f7f7f', '#c7c7c7',
  '#bcbd22', '#dbdb8d',
  '#17becf', '#9edae5'
 ];

class PainterGraph{

   /*constructor(graph, layout){
        self = this;
        this.graph = graph;
        this.layout = layout;
    }*/

    constructor(graph){
      self = this;
      this.graph = graph;
    }

    painGraph(){
        /*PIXI.js */
        let pixiGraphics = new BuildPixiGraphics(this.graph, {
          //Setting ngraph.pixi
          rendererOptions: {
            backgroundColor: 0x333333,
            antialias: true,
          },
          labelConf: {
            enable: false,
            text: 'Label to personal code'
          },
          velocityLayout: 1,
          //layout: self.layout,
        });
        pixiGraphics.createNodeUI(require('./lib/createNodeUI'));
        pixiGraphics.renderNode(require('./lib/renderNode'));
        pixiGraphics.createLinkUI(require('./lib/createLinkUI'));
        pixiGraphics.renderLink(require('./lib/renderLink'));

        pixiGraphics.run();
    }
}
export default PainterGraph;