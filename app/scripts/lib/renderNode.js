'use strict';

import events from 'ngraph.events'


module.exports = function (animatedNode, ctx) {
  animatedNode.renderFrame();
  ctx.lineStyle(0);
  ctx.beginFill(animatedNode.color,1);
  ctx.drawCircle(animatedNode.pos.x, animatedNode.pos.y, animatedNode.width);

  ctx.on('explore', function(node){
    console.log('nodeId: ' + node);
  })

  ctx.click = function(animatedNode){
    this.fire('explore', animatedNode.node.id);
  };
/*
  if(ctx.addEventNode === undefined || ctx.addEventNode === false){
    ctx.addEventNode = true;
    events(ctx);
    ctx.on('hover', function(){
      var node = animatedNode.node;
      console.log('mouse hover on the node');
      let labelStyle = { fontFamily: "Arial", fontSize: "20px" ,  fill: 0x000000};
      let labelText = node.id;
      if(node.label === undefined){
          node.label = new PIXI.Text(labelText, labelStyle);
          node.label.x = animatedNode.pos.x;
          node.label.y = animatedNode.pos.x;
          ctx.addChild(node.label);
      }else{
          node.label.x = animatedNode.pos.x;
          node.label.y = animatedNode.pos.x;
          node.label.updateText();
      }*/
      /*node.colorNode(0xc7c7c7);
      node.links.forEach(link => {
          link.colorLinks(0x9edae5);
      });*/
   // });
    /*ctx.interactive = true;
    ctx.mouseover = function(){
      this.fire('hover');
    }*/
    
//  }
  
}
