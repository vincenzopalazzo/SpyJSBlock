'use strict';

import BuildLayout from 'ngraph.offline.layout'

class GraphLayoutUtil{

    constructor(graph){
        this.graph = graph;
    }

    buildLayout(){
        let self = this;
        let builder = new BuildLayout(this.graph);
        console.log('Graph: ', this.graph);
        let layout = builder(self.graph, {
            layout: require('ngraph.forcelayout3d') // use custom layouter
          });
        layout.run();
        return layout;
    }
}

export default GraphLayoutUtil;